package pl.training.shop.payments;

import net.bytebuddy.implementation.bytecode.Addition;
import org.javamoney.moneta.FastMoney;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.AdditionalAnswers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FakePaymentServiceTest {

    private static final String PAYMENT_ID = "1";
    public static final FastMoney MONEY = LocalMoney.of(1_000);
    public static final PaymentRequest PAYMENT_REQUEST = PaymentRequest.builder().fastMoney(MONEY).build();

    @Mock
    private PaymentIdGenerator paymentIdGenerator;
    private Payment payment;
    
    @Mock
    private PaymentRepository paymentRepository;

    @BeforeEach
    void setUp(){
        when(paymentIdGenerator.getNext()).thenReturn(PAYMENT_ID);
        when(paymentRepository.save(any(Payment.class))).then(AdditionalAnswers.returnsFirstArg());
        FakePaymentService fakePaymentService = new FakePaymentService(paymentIdGenerator, paymentRepository);
        payment = fakePaymentService.process(PAYMENT_REQUEST);
    }

    @DisplayName("Should assign generated id to created payment")
    @Test
    void shouldAssignGeneratedIdToCreatedPayment(){
        assertEquals(PAYMENT_ID, payment.getId());
    }

    @DisplayName("Should assign money from payment request to created payment")
    @Test
    void shouldAssignMoneyFromPaymentRequestToCreatedPayment(){
        assertEquals(MONEY, payment.getMoney());
    }

    @DisplayName("Should save created payment")
    @Test
    void shouldSaveCreatedPayment(){
        verify(paymentRepository).save(payment);
    }

    @DisplayName("Should save created payment")
    @Test
    void asdf(){
        List<String> str = new ArrayList<>();

        System.out.println(str.stream().anyMatch(x -> x.equals("asd")));
    }



}