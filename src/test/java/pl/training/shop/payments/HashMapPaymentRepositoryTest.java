package pl.training.shop.payments;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;
import java.util.Optional;

import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
public class HashMapPaymentRepositoryTest {

    private static String PAYMENT_ID = "1";
    public static final Payment PAYMENT = Payment.builder()
            .id(PAYMENT_ID)
            .build();

    private final HashMapPaymentRepository paymentRepository = new HashMapPaymentRepository();

    @Mock
    private Map<String, Payment> payments;

    @BeforeEach
    void setUp(){
        paymentRepository.setDb(payments);
    }

    @DisplayName("Shou;d add payment to hash map under payment id")
    @Test
    void shouldAddPaymentToHashMapUnderPaymentId(){
        paymentRepository.save(PAYMENT);
        verify(payments).put(PAYMENT_ID, PAYMENT);
    }

    @Test
    void test(){
        Long x = 1L;
        Optional<Long> x1 = Optional.ofNullable(x);


        System.out.println(x1.orElseGet(() -> 100L));
    }

}