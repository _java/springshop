package pl.training.shop.payments;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Getter
@Repository("paymentRepository")
public class HashMapPaymentRepository implements PaymentRepository {
    @Setter
    private Map<String, Payment> db = new HashMap<>();

    @Override
    public Payment save(Payment payment) {
        db.put(payment.getId(), payment);
        return payment;
    }
}
