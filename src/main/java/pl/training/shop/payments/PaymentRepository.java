package pl.training.shop.payments;

import org.springframework.stereotype.Repository;

public interface PaymentRepository {
    Payment save(Payment payment);
}
