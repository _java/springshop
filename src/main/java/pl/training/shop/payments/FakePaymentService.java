package pl.training.shop.payments;


import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.Instant;

@Log
@Service("paymentService")
@RequiredArgsConstructor
public class FakePaymentService implements PaymentService {

    private PaymentIdGenerator paymentIdGenerator;
    private PaymentRepository paymentRepository;

    //jesli nazwa beana byłaby inna niż paymentIdGenerator, wtedy potrzebny byłby qualifier() ze względu na konflit beanów
        public FakePaymentService(@Qualifier("incrementalPaymentIdGenerator") PaymentIdGenerator paymentIdGenerator, PaymentRepository paymentRepository) {
        this.paymentIdGenerator = paymentIdGenerator;
        this.paymentRepository = paymentRepository;
    }

    @LogPayments
    @Override
    public Payment process(PaymentRequest paymentRequest){
        Payment paymentResult = Payment.builder()
                .id(paymentIdGenerator.getNext())
                .money(paymentRequest.getFastMoney())
                .timestamp(Instant.now())
                .status(PaymentStatus.STARTED)
                .build();
        return paymentRepository.save(paymentResult);
    }

}

